# Mvp Hosts utility
This is a conviniant script for inserting, updating and removing the MvpHost lines found on: http://winhelp2002.mvps.org/hosts.txt, to your hosts file
The mvpHosts lines are usefull in that they redirect known unwanted hosts to ip address 0.0.0.0

Read about them here: http://winhelp2002.mvps.org

## Usage
* sudo go run mvpHosts.go -insert, This will insert or update the mvpHosts lines in your hosts file
* sudo go run mvpHosts.go -remove, This will remove the mvpHosts lines in your hosts file

### Make executeble 
* go build mvpHosts.go
* mv mvpHosts to the /usr/bin 
* Use it like so: sudo mvpHosts -insert or mvpHosts -remove

## NOTE
I wrote this to work on Linux devices

